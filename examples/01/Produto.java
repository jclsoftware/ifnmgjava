/*
* Classe que representa um produto
* Não é executavel.
* Tem métodos (com e sem retorno) e atributos
* Tem pelo menos uma classe interna
* Ter um método toString
* Justificar a escolha dos modificadores dos métodos (private, protect e public),
* o uso ou não de exceções e método estático (pelo menos 1)
*/

public class Produto {
	//Declarar atributos (propriedades)
	int codigo;
	//Ex: nome="Jeancarlo";
	String nome;
	//Construtor
	//que altera algum atributo do objeto
	public Produto(int cod, String nome)throws Exception{
		setCodigo(cod);
		setNome(nome);
	}
	//Métodos
	// que retorna valor (boolean)
	// O que evitar: return (nome.length() >= 3)	
	// estático
	public static boolean validarNome(String novo){
		boolean resultado=false;
		if(novo.length() > 2){
			resultado = true;
		}else if(novo.toUpperCase()==novo.toString()){
			//SE("JEANCARLO"=="Jeancarlo");
			if(novo.length() > 1){
				if(novo.length() > 1){
					resultado = true;
				}
			}
		}
		return resultado;
	}
	//Metodos set e get
	public String getNome(){
		return nome;
	}
	public int getCodigo(){
		return codigo;
	}
	//Método protect
	//que lança exceção
	protected void setNome(String novo)throws Exception{
		if(validarNome(novo)==true){
			this.nome = novo;
		}else {
			throw new NomeInvalidoException();
		}
	}
	// private
	private void setCodigo(int cod){
		this.codigo = cod;
	}
	public String toString(){
		return getCodigo() + "; " + getNome();
	}
}
//Uma exceção personalizada
class NomeInvalidoException extends Exception {
	public String toString(){
		return "Nome inválido!";
	}
}