/**
* Uma classe responsavel por listar os produtos
* 
*/
public class ListaDeProdutos {
	public static void main(String [] arg){
		try{
			Produto [] produtos = new Produto[]{
				new Produto(1, "Biscoito"),
				new Produto(2, "Bolacha"),
				new Produto(3, "Cookie"),
				new Produto(4, "Biscuit"),
				new Produto(5, "Brownie"),
				new Produto(6, "Passatempo"),
				new Produto(6, "Ov")
			};
			for(Produto item: produtos){
				System.out.println(item.toString());
			}
		}catch(Exception ex){
			System.out.println(ex);
		}
	}
}